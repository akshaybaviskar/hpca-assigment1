#include <stdint.h>
#include <stdlib.h>
#include <iostream>
#include <ctime>
#include <cmath>
#include <fstream>
using namespace std;

#define FILTER_SIZE 3
#define FILTERS 2


// Optimize this function
void applyFilters(int *image, int height, int width, int *filter, int filters)
{
    int *copy = new int[height * width];
    int h = 0;
	
	/*Apply filter1 on first row of image.
	  Also 
	*/
    for(int w = 0; w < width;++w)
    {
        copy[w] = 0;
	if(w==0)
	{
		copy[w] += image[w] 		* filter[4];
		copy[w] += image[ 1 + w] 	* filter[5];
		copy[w] += image[width +  0 + w] * filter[7];
		copy[w] += image[width +  1 + w] * filter[8];
	}
	else if(w== (width-1))
	{
		copy[w] += image[ -1 + w] 	* filter[3];
		copy[w] += image[w] 		* filter[4];
		copy[w] += image[width + -1 + w]* filter[6];
		copy[w] += image[width +      w] * filter[7];
	}
	else 
	{
		copy[w] += image[ -1 + w] 	* filter[3];
		copy[w] += image[w] 		* filter[4];
		copy[w] += image[ 1 + w] 	* filter[5];
		copy[w] += image[width + -1 + w]* filter[6];
		copy[w] += image[width +  0 + w] * filter[7];
		copy[w] += image[width +  1 + w] * filter[8];
	}	
    }

	/*
	  Apply filter1 on row 2 to last row of image.
	  Parallely apply filter2 on row1 to last but one row of copy and save result back to image.
	*/
    for(h = 1; h < height; ++h)
    {
      for(int w = 0; w <= width;++w)
      {
	  if(w!=width)
	  {
		  /*Take out repetative calculations.*/
		  /*Apply filter1*/
        int x = h * width + w;
		int h_w = h * width;
		int h_w_plus_width = h_w + width;
		int h_w_minus_width = h_w - width;
		int w_minus_1 = w-1;
		int w_plus_1 = w+1;

	    copy[x] = 0;

		if(h==(height-1))
		{
			if(w!=0)
			{
			copy[x] += image[h_w_minus_width + w_minus_1] * filter[0];
			copy[x] += image[h_w             + w_minus_1] * filter[3];
			}
			copy[x] += image[h_w_minus_width + w] * filter[1];
			copy[x] += image[h_w             + w] * filter[4];
			if(w!=(width-1))
			{
			copy[x] += image[h_w             +  w_plus_1] * filter[5];
			copy[x] += image[h_w_minus_width +  w_plus_1] * filter[2];
			}
		}
		else
		{
			if(w!=0)
			{
			copy[x] += image[h_w_minus_width + w_minus_1] * filter[0];
			copy[x] += image[h_w             + w_minus_1] * filter[3];
			copy[x] += image[h_w_plus_width  + w_minus_1] * filter[6];
			}					
			copy[x] += image[h_w_minus_width + w] * filter[1];
			copy[x] += image[h_w             + w] * filter[4];
			copy[x] += image[h_w_plus_width  + w] * filter[7];
			if(w!=(width-1))
			{
			copy[x] += image[h_w_minus_width +  w_plus_1] * filter[2];
			copy[x] += image[h_w             +  w_plus_1] * filter[5];
			copy[x] += image[h_w_plus_width  +  w_plus_1] * filter[8];
			}
		}			
	 }
	
	/*Take out repetative calculations.*/
	/*Apply filter2*/
	int img_h = h-1;
	int img_w = w-1;
	int img_w_minus_1 = w-2;
	int img_h_w = img_h * width;
	int y = img_h_w + img_w;
	int img_h_w_plus_width = img_h_w + width;
	int img_h_w_minus_width = img_h_w - width;
	if(w!=0)
	{
		image[y] = 0;
		if(img_h==0)
		{
			if(img_w!=0)
			{
			image[y] += copy[img_h_w            + img_w_minus_1] * filter[12];
			image[y] += copy[img_h_w_plus_width + img_w_minus_1] * filter[15];
			}
			image[y] += copy[img_h_w            + img_w] * filter[13];
			image[y] += copy[img_h_w_plus_width + img_w] * filter[16];
			if(img_w!=(width-1))
			{
			image[y] += copy[img_h_w            +  w] * filter[14];
			image[y] += copy[img_h_w_plus_width +  w] * filter[17];
			}
		}
		else
		{
			if(img_w!=0)
			{
			image[y] += copy[img_h_w_minus_width  + img_w_minus_1] * filter[ 9];
			image[y] += copy[img_h_w              + img_w_minus_1] * filter[12];
			image[y] += copy[img_h_w_plus_width   + img_w_minus_1] * filter[15];
	  		}
			image[y] += copy[img_h_w_minus_width  +  img_w] * filter[10];
			image[y] += copy[img_h_w              +  img_w] * filter[13];
			image[y] += copy[img_h_w_plus_width   +  img_w] * filter[16];
			if(img_w!=(width-1))
			{
			image[y] += copy[img_h_w_minus_width +  w] * filter[11];
			image[y] += copy[img_h_w             +  w] * filter[14];
			image[y] += copy[img_h_w_plus_width  +  w] * filter[17];
			}
		}
        }
      }
   }

    /*Apply filter2 on last row of copy.*/
    h = height;
    for(int w = 0; w < width;++w)
    {
	int y = (h-1) * width + w;
        image[y] = 0;
	if(w==0)
	{
              image[y] += copy[(-1 + (h-1)) * width + 0 + w] * filter[10];
              image[y] += copy[(-1 + (h-1)) * width + 1 + w] * filter[11];
              image[y] += copy[(0 + (h-1)) * width + 0 + w] * filter[13];
              image[y] += copy[(0 + (h-1)) * width + 1 + w] * filter[14];

	}
	else if(w == width -1)
	{
              image[y] += copy[(-1 + (h-1)) * width + -1 + w] * filter[ 9];
              image[y] += copy[(-1 + (h-1)) * width + 0 + w] * filter[10];
              image[y] += copy[(0 + (h-1)) * width + -1 + w] * filter[12];
              image[y] += copy[(0 + (h-1)) * width + 0 + w] * filter[13];
	}
	else
	{
              image[y] += copy[(-1 + (h-1)) * width + -1 + w] * filter[ 9];
              image[y] += copy[(-1 + (h-1)) * width + 0 + w] * filter[10];
              image[y] += copy[(-1 + (h-1)) * width + 1 + w] * filter[11];
              image[y] += copy[(0 + (h-1)) * width + -1 + w] * filter[12];
              image[y] += copy[(0 + (h-1)) * width + 0 + w] * filter[13];
              image[y] += copy[(0 + (h-1)) * width + 1 + w] * filter[14];
	}
    }

    delete copy;
}

int main()
{
    int ht, wd;
    cin >> ht >> wd;
    int *img = new int[ht * wd];
    for(int i = 0; i < ht; ++i)
        for(int j = 0; j < wd; ++j)
            cin >> img[i * wd + j];
            
    int filters = FILTERS;
    int *filter = new int[filters * FILTER_SIZE * FILTER_SIZE];
    for(int i = 0; i < filters; ++i)
        for(int j = 0; j < FILTER_SIZE; ++j)
            for(int k = 0; k < FILTER_SIZE; ++k)
                cin >> filter[i * FILTER_SIZE * FILTER_SIZE + j * FILTER_SIZE + k];
    
    clock_t begin = clock();
    applyFilters(img, ht, wd, filter, filters);
    clock_t end = clock();
    cout << "Execution time: " << double(end - begin) / (double)CLOCKS_PER_SEC << " seconds\n";
    ofstream fout("output.txt");
    for(int i = 0; i < ht; ++i)
    {
        for(int j = 0; j < wd; ++j)
            fout << img[i * wd + j] << " ";
        fout << "\n";
    }
}


/*
	Original:
	Execution time: 15.5017 seconds
        34,623,585      branch-misses                                                 (37.50%)
        54,604,866      cache-misses                                                  (50.00%)
    92,666,534,926      L1-dcache-loads                                               (49.99%)
     1,337,916,733      L1-dcache-load-misses     #    1.44% of all L1-dcache hits    (25.00%)
       902,942,338      LLC-loads                                                     (25.00%)
        45,228,923      LLC-load-misses           #    5.01% of all LL-cache hits     (37.51%)
       140,241,152      LLC-stores                                                    (25.00%)
        10,588,159      LLC-store-misses                                              (25.00%)

	Change 1: Make filter update row-wise 
	Execution time: 10.7896 seconds
        34,521,958      branch-misses                                                 (37.50%)
        24,107,329      cache-misses                                                  (50.00%)
    92,474,449,674      L1-dcache-loads                                               (50.00%)
       633,493,616      L1-dcache-load-misses     #    0.69% of all L1-dcache hits    (25.00%)
       302,358,549      LLC-loads                                                     (25.00%)
        12,779,385      LLC-load-misses           #    4.23% of all LL-cache hits     (37.50%)
       137,144,399      LLC-stores                                                    (25.00%)
        10,934,825      LLC-store-misses                                              (25.00%)

	Change 2: Make copy to image copy row-wise
	Execution time: 7.90716 seconds
	34,476,997      branch-misses                                                 (37.50%)
         6,970,032      cache-misses                                                  (50.00%)
    92,593,511,220      L1-dcache-loads                                               (50.00%)
       113,312,748      L1-dcache-load-misses     #    0.12% of all L1-dcache hits    (25.01%)
         8,989,108      LLC-loads                                                     (25.01%)
         3,696,290      LLC-load-misses           #   41.12% of all LL-cache hits     (37.51%)
         4,106,172      LLC-stores                                                    (25.00%)
         3,138,963      LLC-store-misses                                              (24.99%)

	Change 3: Avoid copy to image copy
	 Execution time: 7.441 seconds	
	 33,750,163      branch-misses                                                 (37.50%)
         4,997,034      cache-misses                                                  (50.01%)
    89,710,256,245      L1-dcache-loads                                               (50.00%)
        95,085,313      L1-dcache-load-misses     #    0.11% of all L1-dcache hits    (25.01%)
        10,686,258      LLC-loads                                                     (25.00%)
         2,917,981      LLC-load-misses           #   27.31% of all LL-cache hits     (37.50%)
         2,475,268      LLC-stores                                                    (25.00%)
         1,963,584      LLC-store-misses                                              (24.99%)
	
	Change 4: Put filter inside
	 Execution time: 7.41136 seconds
         32,249,962      branch-misses                                                 (37.50%)
         5,169,195      cache-misses                                                  (50.00%)
    89,733,918,076      L1-dcache-loads                                               (50.00%)
        92,182,090      L1-dcache-load-misses     #    0.10% of all L1-dcache hits    (25.01%)
         9,354,236      LLC-loads                                                     (25.00%)
         3,325,023      LLC-load-misses           #   35.55% of all LL-cache hits     (37.51%)
         2,354,774      LLC-stores                                                    (25.00%)
         1,810,034      LLC-store-misses                                              (24.99%)

	Change 5: Unroll first loop
	Execution time: 5.59297 seconds
        33,957,737      branch-misses                                                 (37.50%)
         5,230,395      cache-misses                                                  (50.00%)
    79,711,547,667      L1-dcache-loads                                               (50.00%)
        90,581,366      L1-dcache-load-misses     #    0.11% of all L1-dcache hits    (25.00%)
         9,208,630      LLC-loads                                                     (25.00%)
         3,237,530      LLC-load-misses           #   35.16% of all LL-cache hits     (37.50%)
         2,526,864      LLC-stores                                                    (25.00%)
         1,974,914      LLC-store-misses                                              (25.00%)

	Change 6: Unroll second loop
	Execution time: 3.61533 seconds
        33,692,405      branch-misses                                                 (37.50%)
         4,988,690      cache-misses                                                  (50.00%)
    69,653,222,282      L1-dcache-loads                                               (50.00%)
        87,212,941      L1-dcache-load-misses     #    0.13% of all L1-dcache hits    (25.01%)
         8,538,018      LLC-loads                                                     (25.00%)
         3,029,160      LLC-load-misses           #   35.48% of all LL-cache hits     (37.50%)
         2,361,374      LLC-stores                                                    (25.00%)
         1,943,014      LLC-store-misses                                              (25.00%)

	Change 7 : Remove repeated calculations
	Execution time: 3.03626 seconds
        33,710,678      branch-misses                                                 (37.49%)
         6,198,482      cache-misses                                                  (49.99%)
    64,803,510,313      L1-dcache-loads                                               (49.98%)
        90,995,332      L1-dcache-load-misses     #    0.14% of all L1-dcache hits    (25.00%)
        10,258,265      LLC-loads                                                     (25.01%)
         3,691,186      LLC-load-misses           #   35.98% of all LL-cache hits     (37.52%)
         2,818,663      LLC-stores                                                    (25.02%)
         2,139,650      LLC-store-misses                                              (25.00%)

      17.717581471 seconds time elapsed

*/

